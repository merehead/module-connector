<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/10/18
 * Time: 3:35 PM
 */

namespace MereHead\ModuleConnector\WalletsServices;


trait BankAccountService
{
    public function getBankAccounts(int $account_id)
    {
        $msg = [
            'commands' => __TRAIT__ . '@' . __FUNCTION__,
            'data' => [
                'account_id' => $account_id
            ]
        ];
        return $this->makeCall($msg);
    }

    public function bankAccountStore(int $account_id, $account_number, $bank_name, $holder_name, $bsb, $default)
    {
        $msg = [
            'commands' => __TRAIT__ . '@store',
            'data' => [
                'account_id' => $account_id,
                'account_number' => $account_number,
                'bank_name' => $bank_name,
                'holder_name' => $holder_name,
                'bsb' => $bsb,
                'default' => $default,
            ]
        ];
        return $this->makeCall($msg);
    }

    public function bankAccountUpdate(int $account_id, int $id, $account_number, $bank_name, $holder_name, $bsb, $default)
    {
        $msg = [
            'commands' => __TRAIT__ . '@update',
            'data' => [
                'account_id' => $account_id,
                'id' => $id,
                'account_number' => $account_number,
                'bank_name' => $bank_name,  
                'holder_name' => $holder_name,
                'bsb' => $bsb,
                'default' => $default,
            ]
        ];
        return $this->makeCall($msg);
    }

    public function bankAccountDelete(int $account_id) {
        $msg = [
            'commands' => __TRAIT__ . '@delete',
            'data' => [
                'account_id' => $account_id
            ]
        ];
        return $this->makeCall($msg);
    }

}