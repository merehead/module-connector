<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/15/18
 * Time: 4:44 PM
 */

namespace MereHead\ModuleConnector\TradeServices;


trait BalanceService
{


    /**
     * Command for listening : get_balances
     * Get user balance
     * @param int $accountId
     * @return mixed
     */
    public function getBalances(int $accountId){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
            ],
        ];

        return $this->makeCall($msg);
    }


    /***
     * Set balance for user
     * @param int $accountId
     * @param int $assetId
     * @param $balance
     * @return mixed
     */
    public function setBalance(int $accountId, int $assetId, $balance){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'asset_id' => $assetId,
                'balance' => $balance
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Get balance user for specific asset
     * @param int $accountId
     * @param int $assetId
     * @return mixed
     */
    public function getBalance(int $accountId, int $assetId){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'asset_id' => $assetId,
            ],
        ];
        return $this->makeCall($msg);
    }

    /**
     * Set frozen balance for user
     * @param int $accountId
     * @param int $assetId
     * @param $amount
     * @return mixed
     */
    public function setFrozenBalance(int $accountId, int $assetId, $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'asset_id' => $assetId,
                'amount' => $amount
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : increase_balance
     * Increase user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function increaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];

        return $this->makeCall($msg);
    }


    /**
     * Command for listening : freeze_balance
     * Freeze user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function freezeBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];
        return $this->makeCall($msg);
    }

    /**
     * Command for listening : decrease_balance
     * Decrease user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function decreaseBalance(int $assetId, int $accountId, float $amount){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'asset_id' => $assetId,
                'account_id' => $accountId,
                'amount' => $amount,
            ],
        ];
        return $this->makeCall($msg);
    }


    /**
     * Get user wallets
     * @param int $accountId
     * @return mixed
     */
    public function getUserWalletsByBalance(int $accountId){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
            ],
        ];
        return $this->makeCall($msg);
    }
}