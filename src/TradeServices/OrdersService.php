<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:12 PM
 */

namespace MereHead\ModuleConnector\TradeServices;


trait OrdersService
{

    /**
     * Command for listening : getOrdersBook
     * Get orders book
     * @param string $pair it's get parameters like btc_ltc
     * @return mixed
     */
    public function getOrdersBook(string $pair)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'pair' => $pair,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : getTrades
     * Get trades
     * @param string $pair it's get parameters like btc_ltc
     * @return mixed
     */
    public function getTrades(string $pair)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'pair' => $pair,
            ],
        ];

        return $this->makeCall($msg);
    }


    /**
     * Command for listening : get_orders
     * Get user order
     * @param int $accountId
     * @param string|null $pair
     * @param string|null $status
     * @param int $current_page
     * @param int $per_page
     * @return mixed
     */
    public function getOrders(int $accountId, string $pair = null, string $status = null, int $current_page = 0, int $per_page = 15)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'pair' => $pair,
                'status' => $status,
                'current_page' => $current_page,
                'per_page' => $per_page
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : get_transactions
     * Get user transactions
     * @param int $accountId
     * @return mixed
     */
    public function getTransactions(int $accountId)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
            ],
        ];
        return $this->makeCall($msg);
    }

    /**
     * Command for listening : calculate_limit_order
     * Calculate user limit order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $price
     * @param float $quantity
     * @return array
     */
    public function calculateLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'price' => $price,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : calculate_market_order
     * Calculate market order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $quantity
     * @return array
     */
    public function calculateMarketOrder(int $accountId, string $type, string $pair, float $quantity)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : create_limit_order
     * Create user limit order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $price
     * @param float $quantity
     * @return array
     */
    public function createLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'price' => $price,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : create_market_order
     * Create market order
     * @param $accountId
     * @param $type
     * @param $pair
     * @param $quantity
     * @return array
     */
    public function createMarketOrder( $accountId, $type, $pair, $quantity)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $accountId,
                'type' => $type,
                'pair' => $pair,
                'quantity' => $quantity,
            ],
        ];

        return $this->makeCall($msg);
    }

    /**
     * Command for listening : cancel_order
     * Cancel user order
     * @param $user_id
     * @param $orderId
     * @return array
     */
    public function cancelOrder($user_id, $orderId)
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $user_id,
                'order_id' => $orderId,
            ],
        ];
        return $this->makeCall($msg);
    }



    /**
     *  Command for listening : get_orders_transactions
     * Get orders transactions
     * @param $user_id
     * @return array
     */
    public function getOrdersTransactions($user_id){
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [
                'account_id' => $user_id
            ],
        ];

        return $this->makeCall($msg);
    }


}