<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:15 PM
 */

namespace MereHead\ModuleConnector\TradeServices;


trait AssetsService
{


    /**
     * Command for listening : get_assets
     * Get list assets
     * @return mixed
     */
    public function getAssets()
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__
        ];
        return $this->makeCall($msg);
    }

    /**
     * Command for listening : get_assets_pairs
     * Get list assets pairs
     * @return mixed
     */
    public function getAssetsPairs()
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__
        ];
        return $this->makeCall($msg);
    }


    /**
     * Command for listening : get_assets_pairs_codes
     * Get list assets pairs codes
     * @return array
     */
    public function getAssetsPairsCodes()
    {
        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__
        ];
        return $this->makeCall($msg);
    }


    /**
     *  Command for listening : ger_hot_wallet_balance
     * Get hot wallet balances
     * @return array
     */
    public function getHotWalletBalance()
    {

        $msg = [
            'command' => __TRAIT__.'@'.__FUNCTION__,
            'data' => [ ],
        ];

        return $this->makeCall($msg);
    }
}