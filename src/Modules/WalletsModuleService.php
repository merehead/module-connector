<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/9/18
 * Time: 6:24 PM
 */
namespace  MereHead\ModuleConnector\Modules;

use MereHead\ModuleConnector\WalletsServices\BankAccountService;
use MereHead\ModuleConnector\WalletsServices\UserService;
use MereHead\ModuleConnector\WalletsServices\WalletsService;
use MereHead\ModuleConnector\WalletsServices\CardService;

class WalletsModuleService extends WalletsConnectionModule
{

    use WalletsService, BankAccountService, CardService, UserService;

    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        return $this->makeCall($msg);
    }

}