<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/19/18
 * Time: 2:51 PM
 */

namespace  MereHead\ModuleConnector\Modules;

/**
 * Class WalletsConnectionModule
 * @package App\Services\Modules
 */
class WalletsConnectionModule implements BaseModuleConnector
{

    protected $requester;

    //default connection to Trade-module
    private $dns = "tcp://localhost:5557";

    //check is connected
    protected $connected = false;

    function __construct()
    {
        $context = new \ZMQContext;
        try {
            $this->requester = new \ZMQSocket($context, \ZMQ::SOCKET_REQ);
            $this->connect();
        } catch (\ZMQSocketException $e) {
            throw \Exception('zmq don\'t connected to client');
        }
    }


    public function isConnected() : bool
    {
        try {
            $endpoints = $this->requester->getEndpoints();
            if (in_array($this->dns, $endpoints['connect'])) {
                return true;
            } else {
                return false;
            }
        } catch (\ZMQSocketException $e) {
            return false;
        }
    }


    public function makeCall(array $msg) : array
    {
        try {
            $this->requester->send(json_encode($msg));
            $reply = json_decode($this->requester->recv(), 1);
            if (empty($reply)){
                return ['status' => false, 'error' => 'message is null'];
            }
            return $reply;
        } catch (\ZMQSocketException $e) {
            return [ 'status' => false, 'error' => $e->getMessage()];
        }
    }

    public function connect(): bool
    {
        try {
            $this->requester->setSockOpt(\ZMQ::SOCKOPT_LINGER, 0);
            $this->requester->setSockOpt(\ZMQ::SOCKOPT_RCVTIMEO, 100);
            $this->requester->setSockOpt(\ZMQ::SOCKOPT_SNDTIMEO, 100);
            if (!empty(env('walletsModuleDns'))) {
                $this->requester->connect(env('walletsModuleDns'));
            } else {
                $this->requester->connect($this->dns);
            }
            $this->connected = true;
        }catch (\ZMQSocketException $e) {
            $this->connected = false;
        }
        return $this->connected;
    }


    public function getAddress(): string
    {
        return $this->dns;
    }


    public function setAddress(string $dns)
    {
        $this->dns = $dns;
    }

}