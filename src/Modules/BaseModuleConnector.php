<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/19/18
 * Time: 2:44 PM
 */

namespace  MereHead\ModuleConnector\Modules;


/**
 * Base method who describe modules
 * Interface BaseModule
 * @package App\Services\Modules
 */
interface BaseModuleConnector
{
    /**
     * Check is connected module
     * @return bool
     */
    public function isConnected() : bool;


    /**
     * Get client address who will be use for connection
     * @return bool
     */
    public function getAddress() : string;


    /**
     *  Connect to client if isn't connected
     * @return bool
     */
    public function connect() : bool;

    /**
     * Function for creating call to client
     * @return mixed
     */
    public function makeCall(array $msg) : array;


    /**
     * Set dns client address
     * @param string $dns
     * @return mixed
     */
    public function setAddress(string $dns);
}